#include "widget.h"
#include <QPainter>
#include <QLine>
#include <QPainterPath>

Widget::Widget(QWidget *parent)
    : QWidget{parent}
{
    setMouseTracking(1);
    clear();
}

void Widget::paintEvent(QPaintEvent *){
    if(isDrawing){
        QPointF temp(mapFromGlobal(cursor().pos()));
        QPainter painter(points);
        painter.setPen(QPen(color,size,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin));
        if(prev.isNull()){
            painter.drawPoint(temp);
            prev = temp;
        }
        painter.drawLine(temp,prev);
        prev = temp;
    }
    QPainter ppainter(this);
    ppainter.drawPixmap(rect(), *points);
    QPainter curpainter(this);
    curpainter.setPen(QPen(Qt::lightGray,1));
    curpainter.drawEllipse(mapFromGlobal(cursor().pos()),size/2,size/2);
}

void Widget::mousePressEvent(QMouseEvent*){
    isDrawing = 1;
    update();
}

void Widget::mouseMoveEvent(QMouseEvent*){
//    if(isDrawing){
//        update();
//    }
    update();
}

void Widget::mouseReleaseEvent(QMouseEvent*){
    prev.rx() = NULL;
    prev.ry() = NULL;
    isDrawing = 0;
}

void Widget::clear(){
    points = new QPixmap(rect().width(),rect().height());
    points->fill();
    prev.rx() = NULL;
    prev.ry() = NULL;
    update();
}

