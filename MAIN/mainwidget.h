#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include "widget.h"
#include <QPushButton>
#include <QComboBox>
#include <QColorDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

protected slots:
    void buttonClick();
    void boxClick();
    void colorClick();
    void cpcl();
//    void toolbarClick();

private:
    Ui::MainWidget *ui;
    Widget* widget;
    QPushButton* button;
    QComboBox* box;
    QPushButton* colorChoose;
    QColorDialog* colorPicker;
//    QComboBox* toolbar;
};
#endif // MAINWIDGET_H
