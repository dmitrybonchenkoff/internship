#include "mainwidget.h"
#include "qpainter.h"
#include "ui_mainwidget.h"
#include <QBoxLayout>
#include <QFormLayout>


MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    widget = new Widget(this);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout* bLayout = new QHBoxLayout();
    bLayout->setAlignment(Qt::AlignLeft);

    button = new QPushButton("clear");
    button->setFixedSize(100,50);
    connect(button,SIGNAL(clicked()),SLOT(buttonClick()));
    bLayout->addWidget(button);

    box = new QComboBox();
    box->setFixedSize(100,50);
    box->addItems(QStringList()<<"2"<<"5"<<"10"<<"50"<<"200");
    box->setCurrentIndex(2);
    connect(box,SIGNAL(currentIndexChanged(int)),SLOT(boxClick()));
    bLayout->addWidget(box);

    colorPicker = new QColorDialog(this);
    colorPicker->setCurrentColor(Qt::red);
    colorPicker->hide();
    connect(colorPicker,SIGNAL(colorSelected(const QColor &)),SLOT(cpcl()));

    colorChoose = new QPushButton();
    colorChoose->setFixedSize(100,50);
    QPixmap shiza(30,30);
    shiza.fill(colorPicker->currentColor());
    QIcon ico(shiza);
    colorChoose->setIcon(ico);
    connect(colorChoose,SIGNAL(clicked()),SLOT(colorClick()));
    bLayout->addWidget(colorChoose);

    mainLayout->addLayout(bLayout);
    mainLayout->addWidget(widget);
    setLayout(mainLayout);
    ui->setupUi(this);
}

void MainWidget::buttonClick(){
    widget->clear();
}

void MainWidget::boxClick(){
    widget->setSize(box->currentText().toInt());
}

void MainWidget::colorClick(){
    colorPicker->setVisible(1);
    colorPicker->show();
}

void MainWidget::cpcl(){
    widget->setColor(colorPicker->currentColor());
    QPixmap shiza(30,30);
    shiza.fill(colorPicker->currentColor());
    QIcon ico(shiza);
    colorChoose->setIcon(ico);
}

MainWidget::~MainWidget()
{
    delete ui;
}

