#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPixmap>

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = nullptr);
    void clear();
    void setSize(int s){size = s;}
    void setColor(QColor c){color = c;}

protected:
    void paintEvent(QPaintEvent * event);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent*);

private:
    QPixmap* points;
    QPointF prev;
    bool isDrawing = 0;
    int size = 10;
    QColor color = Qt::red;
};

#endif // WIDGET_H
